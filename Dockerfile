FROM golangci/golangci-lint

ENV PROJECT_DIR=/code
WORKDIR ${PROJECT_DIR}

COPY . ${PROJECT_DIR}

CMD ["golangci-lint", "run"]